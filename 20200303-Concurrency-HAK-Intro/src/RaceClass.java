package src;

import java.util.ArrayList;
import java.util.Date;

public class RaceClass {

	final static int START_N_THREADS = 7;
	
	public static void main(String[] args) {
		
		ArrayList<Thread> threads  = new ArrayList<>(START_N_THREADS);
		
		Thread t = null;
		Object sync = new Object();

		
		
		for (int i = 0; i < START_N_THREADS; i++) {
			t = new Thread(new CounterWorker(sync));
			t.start();		
			threads.add(t);
		}		
		
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.printf("Counter steht bei: %09d",DataStore.getCounter());
	}

}


class DataStore {
	private static int counter = 0;
	public static Date lastWork = null;
	
	public static int getCounter() {
		return counter;
	}
	public static void setCounter(int counter) {
		DataStore.counter = counter;
	}	
}

class CounterWorker implements Runnable {

	final int COUNT_N_TIMES =1000000;
	Object sync;
	
	public CounterWorker(Object sync) {
		this.sync = sync;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < COUNT_N_TIMES; i++) {		
				count();
		}		
	}
	
	private void count() {
		synchronized (sync) {
			DataStore.setCounter(DataStore.getCounter()+1);	
			//DataStore.lastWork = new Date();		
		}		
	}
	
}