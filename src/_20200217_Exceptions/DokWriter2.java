package _20200217_Exceptions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DokWriter2 { 

	public static void main(String[] args) {
		FileWorker.createFile();
	}

}

class FileWorker {

	private static final String FILE_NAME = "my_file";

	public static void createFile() {
		try {
			Files.createFile(Paths.get(FILE_NAME));
		} catch (IOException e) {
			System.err.println("Input Output Problem");
			e.printStackTrace();
		}		
	}	
	
}