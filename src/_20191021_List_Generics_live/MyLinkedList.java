package _20191021_List_Generics_live;

public class MyLinkedList<E> {
	
	private Element<E> firstElement;
	
	public void add(E data){
		Element<E> newElement = new Element<>(data);		
		if(firstElement == null){
			firstElement = newElement;
		}
		else{
			Element<E> lastElement = getLastElement();
			lastElement.setNextElement(newElement);
		}		
	}

	private Element<E> getLastElement() {
		Element<E> currentElement = firstElement;
		while(currentElement.getNextElement() != null){
			currentElement = currentElement.getNextElement();
		}
		return currentElement;
	}
	
	@Override
	public String toString(){
		String returnString = "";
		Element<E> currentElement = firstElement;
		do{
			returnString += currentElement.getData();
			returnString += "\n";
			currentElement = currentElement.getNextElement();
		}while(currentElement != null);
		return returnString;
	}
	
	public void printString(){
		System.out.println(toString());
	}
}
