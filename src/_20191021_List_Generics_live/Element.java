package _20191021_List_Generics_live;

public class Element<D> {
	private D data;
	private Element<D> nextElement;
	
	public Element(D data){
		this.data = data;
	}

	public D getData() {
		return data;
	}

	public void setData(D data) {
		this.data = data;
	}

	public Element<D> getNextElement() {
		return nextElement;
	}

	public void setNextElement(Element<D> nextElement) {
		this.nextElement = nextElement;
	}
	
	
	
}
