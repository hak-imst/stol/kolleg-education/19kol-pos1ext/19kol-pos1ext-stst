package _20200402_Threading_Basics_02;

import java.util.LinkedList;

public class _04_Synchronization {
    public static void main(String[] args) {

        LinkedList<Thread> threads = new LinkedList<>();

        Object sync = new Object();

        threads.add(new NewThread(sync));
        threads.add(new NewThread(sync));
        threads.add(new NewThread(sync));

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(DataShare.counter);
    }
}

class NewThread extends Thread {

    Object syncObject;
    
    public NewThread(Object syncObject){
        this.syncObject = syncObject;
        start();
    }
    public void run(){
        for(int i = 0; i < 1000; i++){
            synchronized(syncObject){
                DataShare.counter++;
            }                
        }
    }
}

class DataShare {
    public static int counter = 0;
}