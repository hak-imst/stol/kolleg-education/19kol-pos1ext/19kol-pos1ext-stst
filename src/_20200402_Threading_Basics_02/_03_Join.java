package _20200402_Threading_Basics_02;

import java.util.LinkedList;

public class _03_Join {
    public static void main(String[] args) {
        LinkedList<Thread> threads = new LinkedList<>();

        threads.add(new NewThread());
        threads.add(new NewThread());
        threads.add(new NewThread());

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(DataShare.counter);
    }
}

class NewThread extends Thread {
    
    public NewThread(){
        start();
    }
    public void run(){
        for(int i = 0; i < 1000; i++){
                DataShare.counter++;
        }
    }
}

class DataShare {
    public static int counter = 0;
}