package _20191007.threads;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RaceConditionLockExecutor {

	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		System.out.println("Hello World");
		ShareLock share = new ShareLock();
		for(int i = 0; i < 10; i++){
			executor.execute(new WorkerLock(share));
		}		
	}

}

class ShareLock{
	
	Long counter; 
	
	Lock lock = new ReentrantLock();
	
	public ShareLock(){
		counter = (long)0;
	}
	public void increaseCounter(){
		lock.lock();
		counter++;
		lock.unlock();
	}
	
	public void printCounter(){		
		lock.lock();		
		System.out.print(counter);
		System.out.print(" - ");
		System.out.println(Thread.currentThread());
		lock.unlock();
		throw new IndexOutOfBoundsException();
	}
}

class WorkerLock implements Runnable{
	
	ShareLock share;
	
	public WorkerLock(ShareLock share){
		this.share = share;
	}

	@Override
	public void run() {
		for(int i = 0; i < 10000; i++){
			share.increaseCounter();
			share.printCounter();
		}
	}
	
}