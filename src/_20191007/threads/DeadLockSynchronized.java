package _20191007.threads;

public class DeadLockSynchronized {

	private static Object lock1 = new Object();
	private static Object lock2 = new Object();

	public static void main(String[] args) {
		Thread thdA = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true)
					synchronized (lock2) {
						synchronized (lock1) {
							System.out.println("first thread in instanceMethod1");
						}
					}
			}
		});

		Thread thdB = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true)
					synchronized (lock1) {
						synchronized (lock2) {
							System.out.println("second thread in instanceMethod2");
						}
					}
			}
		});
		thdA.start();
		thdB.start();
	}

}
