package _20191007.threads;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadLockLock {

	static Lock lock1 = new ReentrantLock();
	static Lock lock2 = new ReentrantLock();

	public static void main(String[] args) {
		Thread thdA = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true){
					lock2.lock();	
					lock1.lock();														
					System.out.println("first thread in instanceMethod1");	
					lock1.unlock();					
					lock2.unlock();					
				}						
			}
		});

		Thread thdB = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true){		
					lock1.lock();
					lock2.lock();
					System.out.println("first thread in instanceMethod2");		
					lock2.unlock();
					lock1.unlock();					
				}
			}
		});
		thdA.start();
		thdB.start();
	}

}
