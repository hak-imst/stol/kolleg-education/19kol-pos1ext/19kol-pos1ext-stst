package _20191007.threads;

import java.util.Date;

public class RaceConditionSynchronized {

	public static void main(String[] args) {
		System.out.println("Hello World");
		Share share = new Share();
		Worker worker1 = new Worker(share);
		Worker worker2 = new Worker(share);
		Worker worker3 = new Worker(share);
		Worker worker4 = new Worker(share);
		Thread thread1 = new Thread(worker1);
		Thread thread2 = new Thread(worker2);
		Thread thread3 = new Thread(worker3);
		Thread thread4 = new Thread(worker4);
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
	}

}

class Share{
	Long counter; 
	public Share(){
		counter = (long)0;
	}
	synchronized void increaseCounter(){
		counter++;
	}
	
	synchronized void printCounter(){		
		System.out.print(counter);
		System.out.print(" - ");
		System.out.println(new Date());
	}
}

class Worker implements Runnable{
	
	Share share;
	
	public Worker(Share share){
		this.share = share;
	}

	@Override
	public void run() {
		for(int i = 0; i < 10000; i++){
			share.increaseCounter();
			share.printCounter();
		}
	}
	
}