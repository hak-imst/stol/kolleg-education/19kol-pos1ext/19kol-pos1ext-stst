package _20200330_Threading_Basics;

public class _02_Runnable {
    public static void main(String[] args) {                
        new NewThread();
        new NewThread();
        new NewThread();
        System.out.println(DataShare.counter);
    }
}

class NewThread extends Thread {
    public NewThread(){
        start();
    }
    public void run(){
        for(int i = 0; i < 1000; i++){
            DataShare.counter++;
        }
    }
}

class DataShare {
    public static int counter = 0;
}