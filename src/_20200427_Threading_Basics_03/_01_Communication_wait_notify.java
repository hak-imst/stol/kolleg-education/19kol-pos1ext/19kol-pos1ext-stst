package _20200427_Threading_Basics_03;

public class _01_Communication_wait_notify {
    public static void main(String[] args) {

        DataShare ds = new DataShare();

        Runnable sender = () -> {
            int counter = 0;
            while(true){
                ds.setDataString(Integer.toString(counter));
                counter++;
            }
        };

        Runnable receiver = () -> {
            while(true){
                ds.getDataString();
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
 
    }

}

class DataShare {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString() {
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        
        String returnVal = dataString;
        System.out.println("Got: "+returnVal);  
        isSet = false;      
        notify();
        return returnVal;
    }

    synchronized public void setDataString(String str) {
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
       this.dataString = str;
       System.out.println("Sent: "+str);  
       isSet = true;
       notify();
    }
}