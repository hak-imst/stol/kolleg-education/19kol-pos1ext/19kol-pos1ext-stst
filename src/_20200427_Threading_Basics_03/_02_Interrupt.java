package _20200427_Threading_Basics_03;

public class _02_Interrupt_prepare {
    public static void main(String[] args) {

        DataShare ds = new DataShare();

        Runnable sender = () -> {
            int counter = 0;
            while (true && ! Thread.currentThread().isInterrupted()) {
                ds.setDataString(Integer.toString(counter));
                counter++;
            }
            System.out.println("Sender Thread is stopping");
        };

        Runnable receiver = () -> {
            while (true  && ! Thread.currentThread().isInterrupted()) {
                ds.getDataString();
            }
            System.out.println("Receiver Thread is stopping");
        };

        Thread senderT = new Thread(sender);
        senderT.start();
        Thread receiverT = new Thread(receiver);
        receiverT.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        senderT.interrupt();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        senderT = new Thread(sender);
        senderT.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        senderT.interrupt();
        receiverT.interrupt();

 
    }

}

class DataShare {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString() {
        while(!isSet && !Thread.currentThread().isInterrupted()){
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }                
        String returnVal = dataString;
        System.out.println("Got: "+returnVal);  
        isSet = false;      
        notify();
        return returnVal;
    }

    synchronized public void setDataString(String str) {
        while(isSet && !Thread.currentThread().isInterrupted()){
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
       if(!isSet){
            this.dataString = str;
            System.out.println("Sent: "+str);  
            isSet = true;
            notify();
       }       
    }
}