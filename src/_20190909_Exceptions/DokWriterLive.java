package _20190909_Exceptions;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DokWriterLive {

	public static void main(String[] args) {
		WriteFile.writeFile("Hello World!");
	}

}

class WriteFile{
	
	public static final String FILE_NAME = "Meine_Datei";
	
	public static void writeFile(String stringToWrite) {
		
		int counter = 0;
		boolean running = true;
		
		while(running && counter < 1000000){			
		
			try {
				Files.createFile(Paths.get(String.format("%s-%03d.txt", FILE_NAME, counter)));
				running = false;
			} catch(FileAlreadyExistsException e){
				System.out.println("Datei vorhanden, zähle automatisch hoch");
				counter++;
			}		
			catch (IOException e) {
				System.err.println("Es gab einen Fehler beim Dateizugriff!");
				running = false;
			}
		
		}
	}
	
}
