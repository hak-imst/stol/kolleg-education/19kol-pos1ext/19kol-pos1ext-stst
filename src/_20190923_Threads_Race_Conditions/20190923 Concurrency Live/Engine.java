import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.concurrent.locks.ReentrantLock; 

public class Engine
{
    public void start(int distance, Object lock, ReentrantLock rLock){
        
            while(CarData.getDistance() < distance){              
                    
                while(Greenfoot.getRandomNumber(1000000) != 0){}
                rLock.lock();
                int distanceCurrent = CarData.getDistance();            
                CarData.setDistance(distanceCurrent+1);
                rLock.unlock();
            }
              
    }
}
