import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.concurrent.locks.ReentrantLock;

public class MyWorld extends World
{
        
    public MyWorld()
    {    
        super(600, 400, 1);
        addObject(new Wall(),600,100);
        addObject(new Car(),0,100);
        
        CarData.setDistance(0);
        
        Object lock = new Object();
        ReentrantLock rLock = new ReentrantLock();
        
        Runnable runnable = () -> {
            Engine engine = new Engine();
            engine.start(600,lock,rLock);
        };
        
        Thread t = new Thread(runnable);
        t.start();
        
        Thread t2 = new Thread(runnable);
        t2.start();
    }
}
