
public class CarData  
{
    private volatile static int distance = 0;
    
    public static int getDistance(){
        return distance;
    }
    
    public static void setDistance(int distance){
        CarData.distance = distance;
    }    
}
